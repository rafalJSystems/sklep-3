const express = require('express');
const router = express.Router();
const Produkt = require('./../models/produkt');

router.get('/:szukaj', (req, res) => {
    Produkt.find({
        nazwa: {
            '$regex': new RegExp(req.params.szukaj, 'i')
        }
    }, (err, items) => {
        if (err) throw err;

        res.json(items);
    });
});

module.exports = router;