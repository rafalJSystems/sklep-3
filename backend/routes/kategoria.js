const express = require('express');
const router = express.Router();
const Kategoria = require('./../models/kategoria');

router.get('/', (req, res) => {
    Kategoria.find({}, { _id: 1, nazwa: 1, slug: 1 }, (err, items) => {
        if (err) throw err;

        res.json(items);
    });
});

router.get('/:slug', (req, res) => {
    Kategoria.findOne({slug: req.params.slug}, (err, item) => {
        if (err) throw err;

        res.json(item);
    });
});

module.exports = router;