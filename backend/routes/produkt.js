const express = require('express');
const router = express.Router();
const Produkt = require('./../models/produkt');

router.get('/', (req, res) => {
    let query = {};
    let options = {
        limit: 6
    };

    if (Object.keys(req.query).length > 0) {
        delete options.limit;
    }

    // kategorie // ?kat=123456789
    // podkategoria // ?pod=34567890
    // cena // ?c=1&c=4 // [1, 4]
    // marka // ?m=ajsndfj
    // wiek // ?w=12?w=24 // [12, 24]

    // ?kat=5678&pod=3456789&c=1&c=2

    if (req.query.kat) {
        query.kategoria = req.query.kat;
    }

    if (req.query.pod) {
        query.podkategoria = req.query.pod;
    }

    if (req.query.c && req.query.c.length === 2) {
        query.cena = {
            '$gte': req.query.c[0],
            '$lte': req.query.c[1]
        }
    }

    if (req.query.w && req.query.w.length === 2) {
        query.wiek = {
            '$gte': req.query.w[0],
            '$lte': req.query.w[1]
        }
    }

    if (req.query.m) {
        query.marka = {
            '$in': req.query.m
        }
    }

    Produkt.find(query, {}, options, (err, items) => {
        if (err) throw err;

        res.json(items);
    });
});

router.get('/:id', (req, res) => {
    Produkt.findOne({ _id: req.params.id}, (err, items) => {
        if (err) throw err;

        res.json(items);
    });
});

module.exports = router;