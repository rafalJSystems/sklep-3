var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/sklep', {
  useMongoClient: true
});

var Kategoria = require('./models/kategoria');
var Produkt = require('./models/produkt');

var kategorie = [
    {
        nazwa: 'Chłopcy',
        podkategorie: [
            {
                nazwa: 'Klocki'
            },
            {
                nazwa: 'Pojazdy'
            },
            {
                nazwa: 'Sport'
            }
        ]
    },
    {
        nazwa: 'Dziewczynki',
        podkategorie: [
            {
                nazwa: 'Laleczki'
            },
            {
                nazwa: 'Pluszaki'
            },
            {
                nazwa: 'Multimedialne'
            }
        ]
    },
    {
        nazwa: 'Edukacyjne',
        podkategorie: [
            {
                nazwa: 'Instrumenty'
            },
            {
                nazwa: 'Nauka czytania'
            },
            {
                nazwa: 'Nauka liczenia'
            }
        ]
    },
    {
        nazwa: '18+',
        podkategorie: [
            {
                nazwa: 'Filmy przyrodnicze'
            },
            {
                nazwa: 'Akcesoria'
            },
            {
                nazwa: 'Suplementy'
            }
        ]
    }
];

let marki = ['Fisher Price', 'Gry Mattel', 'Gry MB', 'Hot Wheels', 'Little Tikes', 'Play Doh', 'PlaySkool'];

let wynik = {};
for (let k of kategorie) {
    let w = k.nazwa;
    let y = [];
    for (let p of k.podkategorie) {
        let z = [];
        for (var i = 0; i < 20; i++) {
            z.push(produkt(p.nazwa, marki[Math.floor(Math.random() * marki.length)]));
        }
        y[p.nazwa] = z;
    }
    wynik[w] = y;
}

function produkt (co, marka) {
    return {
        nazwa: co + ' #' + Math.random().toString(36).substring(7),
        cena: Math.floor(Math.random() * 100) + 50,
        marka: marka,
        opis: 'Litwo! Ojczyzno moja! Ty jesteś jak zdrowie. Ile cię trzeba cenić, ten zamek stał patrząc, dumając wonnymi powiewami kwiatów oddychając oblicze aż kędy pieprz rośnie gdzie chce, wchodzi byle nie mogę na dachu. Wtem pan tak zawsze służy której ramię z flinty strzelać albo sam zjechać do sądów granicznych. Słusznie Woźny umiał komponować iżby je na dachu. Wtem zapadło do bębna tęsknił, siedząc w zamku worończańskim a na Francuza, Że wszyscy poszli za granicę, to mówiąc, że tak Suwarów w powiecie. Lubił bardzo szybko, suwała się rówiennicą a od wiatrów jesieni. Dóm mieszkalny niewielki, lecz w domu wiecznie będzie i książki. Wszystko bieży ku północy, aż cała izba rozległa się nie śmieli. I ogląda sam przyjmować i przymioty. Stąd droga co jasnej bronisz Częstochowy i gałęzie drzewa powiązane społem gdy inni, więcej książkowej nauki. Ale stryj nie myśl żywą i płynnie mówił, i dwie strony: Uciszcie się! woła. Marząc i nazwisko każdego wodza legijonu i po gromie: w okolicy. i sprzeczki. W końcu, stawiła przed laty. To rzekłszy, z harbajtelem zawiązanym w Litwie chodził tępy nie szpieg, a w nieczynności! a w tym bielsze, że słuchał rozmowy odstrychnęli od słońca blasku Świecił się zaczęły wpółgłośne rozmowy. Mężczyźni rozsądzali swe rodzinne duszę utęsknioną do swawoli. Z góry już pomrok mglisty napełniając wierzchołki i na którego widne były świeżo z Polski jak szlachcic obyczaje wtenczas małe dziecię, kiedy znidzie z nich brylant, niby kamień z nowych gości. W takim Litwinka tylko zgadywana w wieku mu bił głośno, i przepraszał Sędziego. Sędzia tuż przy którym wszystko oddychało. Krótkie były świeżo polewane. Tuż myśliwców herbowne klejnoty wyryte i stoi wypisany każdy mimowolnie porządku i jakoby zlewa. I przyjezdny gość, krewny Horeszków daleki przyjechawszy z panem Hrabią sporu. I starzy i kiedy bliżej poznał u progu rękę do nas wytuza. U tej krucze, długie zwijały. Hreczecha. Tu śmiech młodzieży mowę Wojskiego też same szczypiąc trawę ciągnęły powoli pod strzechą zmieścić się i, z dokumentów przekonywał o nim i stryjaszkiem jedno i swoją ważność zarazem poznaje. jak znawcy, ci znowu o tyle, o muzyce, o muzyce, o autorów pytała Tadeusza zdani i objął gospodarstwo. przyrzekł na trzykrólskie święta przesuwają w służbę rządu, by wychowanie niczego nie jest jak pieniądze Żydzi. To nie zabawia przez rozmowy grzeczne z palcami zadzwonił tabakiera ze śmiechu a potem Sędzia z chleba bez ogona jest armistycjum, to mówiąc, że spudłuje. szarak, gracz nie będziesz przy którym wszystko przepasane, jakby czyjegoś przyjścia był to mówiąc, że polskie ubrani wysmukłą postać tylko się kiedyś demokratą. Bo nie postanie! Nazywam się nagle, stronnicy Sokół na jego trwogi wszczęła rzecz daléj w pole, za nim wiedzą, lekce go bronią od kilku młodych od starych zmienia jej oczyma ciekawymi po tobie. Panno Święta, co je posłyszał, znikał nagle taż chętka, nie odmówi. To mówiąc spojrzał zyzem, gdzie panieńskim rumieńcem dzięcielina pała a bij jak zdrowe oblicz gospodarza, gdy potem Sędzia na szaraki! Za moich, panie, czasów podobne wypadki. Już konie gości Daleki krewny pański i jadł. wtem z Podkomorzym przy pełnym kielich nalać i serce mu przed ganek zajechał któryś z tych pól malowanych zbożem rozmaitem wyzłacanych pszenicą, posrebrzanych żytem. Gdzie bursztynowy świerzop, gryka jak bilardowa kula toczyła się pan Wojski poznał u niego ze cztery. Tymczasem na polowanie i opisuję, bo tak Suwarów w jedno puste miejsce wejrzenie odgadnął zaraz, czyim miało być siedzeniem. Rumienił się, toczył zdumione źrenic po ojcu Podkomorzy i tuż przy którym świecą gęste kutasy jak sługom należy chartu Sokołowi. Pytano zdania innych. więc choć przez kwiaty na jutro na kwaterze pan Hrabia chciał coś mówić, przepraszać, tylko są architektury. Choć Sędzia nigdy nie poruczy, bo tak nazywano młodzieńca, który go.',
        wiek: Math.floor(Math.random() * 6) + 8,
        img_small: 'http://loremflickr.com/320/240',
        img_big: 'http://loremflickr.com/960/300',
    }
};


Kategoria.remove({}, () => {
    Produkt.remove({}, () => {
        let aalalkalka = [];
        for (let k of kategorie) {
            let z = new Kategoria(k);
            aalalkalka.push(z.save());
        }
        Promise.all(aalalkalka).then(() => {
            for (let w in wynik) {
                Kategoria.findOne({nazwa: w}, (err, res) => {
                    let ajdi = res._id;
                    for (let pod of res.podkategorie) {
                        for (let xx in wynik[w][pod.nazwa]) {
                            wynik[w][pod.nazwa][xx].kategoria = ajdi;
                            wynik[w][pod.nazwa][xx].podkategoria = pod._id;
                            let c = new Produkt(wynik[w][pod.nazwa][xx]);
                            c.save();
                        }
                    }
                });
            }
        });
    })
});


