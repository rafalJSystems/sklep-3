import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { RoutingModule } from './routing/routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { SzukajComponent } from './szukaj/szukaj.component';
import { ProduktComponent } from './produkt/produkt.component';
import { KategoriaComponent } from './kategoria/kategoria.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SzukajComponent,
    ProduktComponent,
    KategoriaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
