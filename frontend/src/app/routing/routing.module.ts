import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './../home/home.component';
import { ProduktComponent } from './../produkt/produkt.component';
import { KategoriaComponent } from './../kategoria/kategoria.component';
import { SzukajComponent } from './../szukaj/szukaj.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'produkt/:id', component: ProduktComponent},
  { path: 'kategoria/:slug', component: KategoriaComponent},
  { path: 'szukaj', component: SzukajComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RoutingModule { }
