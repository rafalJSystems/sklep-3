import { Component, OnInit } from '@angular/core';

import { ProduktService } from './../produkt.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ ProduktService ]
})
export class HomeComponent implements OnInit {
  produkty: any;

  constructor(
    private produktService: ProduktService
  ) { }

  ngOnInit() {
    this.produktService.getProdukty().then(result => {
      this.produkty = result;
    });
  }

}
