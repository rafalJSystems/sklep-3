import { TestBed, inject } from '@angular/core/testing';

import { SzukajService } from './szukaj.service';

describe('SzukajService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SzukajService]
    });
  });

  it('should be created', inject([SzukajService], (service: SzukajService) => {
    expect(service).toBeTruthy();
  }));
});
