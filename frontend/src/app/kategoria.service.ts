import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class KategoriaService {
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private apiURL = 'http://localhost:3000/api/kategoria/';

  constructor(private http: Http) { }

  getKategorie(): Promise<any> {
    return this.http.get(this.apiURL)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getKategoria(slug): Promise<any> {
    return this.http.get(this.apiURL + slug)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.log(error);
    return Promise.reject(error);
  }
}
