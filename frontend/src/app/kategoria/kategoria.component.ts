import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { KategoriaService } from './../kategoria.service';
import { ProduktService } from './../produkt.service'; 

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-kategoria',
  templateUrl: './kategoria.component.html',
  styleUrls: ['./kategoria.component.css'],
  providers: [ KategoriaService, ProduktService ]
})
export class KategoriaComponent implements OnInit {
  kategoria: any;
  produkty: any;

  constructor(
    private kategoriaService: KategoriaService,
    private produktService: ProduktService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        return this.kategoriaService.getKategoria(params.get('slug'))
      })
      .subscribe(result => {
        this.kategoria = result;
        this.produktService.getProdukty({search: {kat: result._id}}).then((result) => {
          this.produkty = result;
        });
      });
  }

}
