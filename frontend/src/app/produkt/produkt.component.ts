import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router'

import { ProduktService } from './../produkt.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-produkt',
  templateUrl: './produkt.component.html',
  styleUrls: ['./produkt.component.css'],
  providers: [ ProduktService ]
})
export class ProduktComponent implements OnInit {
  produkt: any;

  constructor(
    private produktService: ProduktService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.switchMap((params: ParamMap) => {
      return this.produktService.getProdukt(params.get('id'));
    })
    .subscribe(result => {
      this.produkt = result;
    });
  }

}
