import { Component, OnInit } from '@angular/core';

import { KategoriaService } from './kategoria.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ KategoriaService ]
})
export class AppComponent implements OnInit {
  menu: any;

  constructor (
    private kategoriaService: KategoriaService
  ) { }

  ngOnInit(): void {
    this.kategoriaService.getKategorie().then((result) => {
      this.menu = result;
    });
  }
}
